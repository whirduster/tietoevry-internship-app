## TietoEvry Internship App

An application that prints out three countries with the lowest and three countries with the highest unemployment rate from given JSON-stat file.

#### Author:
**Tomáš Líbal**

### Prerequisites:

- Install `Java JDK 11` and `Maven`

### Installing the application
- Application can be installed running `$ mvn clean install`

### Running the tests
- Tests can be executed running `$ mvn test`

### Running the application
- Application can be executed running `$ java -jar target/internship-1.0.jar`
