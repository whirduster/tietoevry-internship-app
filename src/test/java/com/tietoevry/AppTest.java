package com.tietoevry;

import com.tietoevry.dataobject.CountryUnemploymentRate;
import com.tietoevry.json.DataExtractor;
import com.tietoevry.json.JsonStatParser;
import no.ssb.jsonstat.v2.Dataset;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AppTest {

    private DataExtractor dataExtractor;

    @BeforeAll
    void setUpDataExtractor() {
        Dataset dataset = null;
        try {
            dataset = JsonStatParser.downloadJsonStatFile("https://json-stat.org/samples/oecd.json");
        } catch (IOException e) {
            fail("Error while downloading json-stat file");
        }
        this.dataExtractor = new DataExtractor(dataset);
    }

    @Test
    void testMapSorting() {
        List<List<String>> keys = new ArrayList<>(dataExtractor.getUnemploymentRate().keySet());
        assertEquals(2.301867378,
                dataExtractor.getUnemploymentRate().get(keys.get(0)),
                "Lowest unemployment rate should be 2.301867378"
        );

        assertEquals(27.2364419,
                dataExtractor.getUnemploymentRate().get(keys.get(keys.size() - 1)),
                "Highest unemployment rate should be 27.2364419"
        );
    }

    @Test
    void testGetLowestUnemploymentRate() {
        ArrayList<CountryUnemploymentRate> unemploymentRates = new ArrayList<>();
        unemploymentRates.add(new CountryUnemploymentRate(
                "Iceland", "IS", 2007, 2.301867378)
        );
        unemploymentRates.add(new CountryUnemploymentRate(
                "Norway", "NO", 2007, 2.498729296)
        );
        unemploymentRates.add(new CountryUnemploymentRate(
                "Mexico", "MX", 2003, 2.998805894)
        );
        List<CountryUnemploymentRate> lowestUnemploymentRates = dataExtractor.getLowestUnemploymentRate(3);

        assertEquals(unemploymentRates, lowestUnemploymentRates,
                "Countries with lowest unemployment rates should be Iceland, Norway and Mexico"
        );
    }

    @Test
    void testGetHighestUnemploymentRate() {
        ArrayList<CountryUnemploymentRate> unemploymentRates = new ArrayList<>();
        unemploymentRates.add(new CountryUnemploymentRate(
                "Greece", "GR", 2014, 27.2364419)
        );
        unemploymentRates.add(new CountryUnemploymentRate(
                "Spain", "ES", 2013, 26.89014696)
        );
        unemploymentRates.add(new CountryUnemploymentRate(
                "Poland", "PL", 2003, 19.61702787)
        );
        List<CountryUnemploymentRate> lowestUnemploymentRates = dataExtractor.getHighestUnemploymentRate(3);

        assertEquals(unemploymentRates, lowestUnemploymentRates,
                "Countries with lowest unemployment rates should be Iceland, Norway and Mexico"
        );
    }

    @Test
    void testGetZeroUnemploymentRates() {
        assertTrue(dataExtractor.getLowestUnemploymentRate(0).isEmpty(),
                "There should not be any output");
        assertTrue(dataExtractor.getHighestUnemploymentRate(0).isEmpty(),
                "There should not be any output");
    }

    @Test
    void testMaxUnemploymentRates() {
        assertEquals(34, dataExtractor.getLowestUnemploymentRate(Integer.MAX_VALUE).size(),
                "There should be 34 countries (we do not care about total of EU15 and OECD)"
        );
        assertEquals(34, dataExtractor.getHighestUnemploymentRate(Integer.MAX_VALUE).size(),
                "There should be 34 countries (we do not care about total of EU15 and OECD)"
        );
    }
}
