package com.tietoevry;

import com.tietoevry.dataobject.CountryUnemploymentRate;
import com.tietoevry.json.DataExtractor;
import com.tietoevry.json.JsonStatParser;
import no.ssb.jsonstat.v2.Dataset;

import java.io.IOException;
import java.util.List;

/**
 * An application that prints out three countries with the lowest
 * and three countries with the highest unemployment rate from given JSON-stat file.
 *
 * @author Tomas Libal
 */
public class App {
    public static void main(String[] args) {
        Dataset oecd = null;
        try {
            oecd = JsonStatParser.downloadJsonStatFile("https://json-stat.org/samples/oecd.json");
        } catch (IOException e) {
            System.err.println("Error while reading input JSON file.");
            System.exit(1);
        }

        DataExtractor extractor = new DataExtractor(oecd);

        System.out.println("Countries with lowest unemployment rate:");
        printResults(extractor.getLowestUnemploymentRate(3));

        System.out.println();

        System.out.println("Countries with highest unemployment rate:");
        printResults(extractor.getHighestUnemploymentRate(3));
    }

    private static void printResults(List<CountryUnemploymentRate> list) {
        for (int i = 0; i < list.size(); i++) {
            CountryUnemploymentRate country = list.get(i);
            System.out.println((i + 1) + ". " + country.getCountryName() + " (" + country.getCountryCode() + ") - " +
                    country.getUnemploymentRate() + "% in year " + country.getYear());
        }
    }
}
