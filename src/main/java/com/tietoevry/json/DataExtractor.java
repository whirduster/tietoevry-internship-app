package com.tietoevry.json;

import com.tietoevry.dataobject.CountryUnemploymentRate;
import no.ssb.jsonstat.v2.Dataset;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class encapsulates methods that extract all necessary data from JSON-file
 *
 * @author Tomas Libal
 */
public class DataExtractor {
    /**
     * Map with unemployment rate values identified by country and year
     */
    private LinkedHashMap<List<String>, Double> unemploymentRate;

    /**
     * Object with loaded JSON-stat file
     */
    private Dataset dataset;

    /**
     * Constructor in which is sorted map of unemployment rate values created
     *
     * @param dataset Object with loaded JSON-stat file
     */
    public DataExtractor(Dataset dataset) {
        unemploymentRate = convertValuesToDouble(dataset.asMap());
        unemploymentRate = sortMapByValue(unemploymentRate);
        this.dataset = dataset;
    }

    public LinkedHashMap<List<String>, Double> getUnemploymentRate() {
        return unemploymentRate;
    }

    public void setUnemploymentRate(LinkedHashMap<List<String>, Double> unemploymentRate) {
        this.unemploymentRate = unemploymentRate;
    }

    /**
     * Finds given number of countries with lowest unemployment rate
     *
     * @param count Number of countries
     * @return Countries with lowest unemployment rate
     */
    public List<CountryUnemploymentRate> getLowestUnemploymentRate(int count) {
        List<List<String>> keys = new ArrayList<>(unemploymentRate.keySet());
        return getCountryList(count, keys);
    }

    /**
     * Finds given number of countries with highest unemployment rate
     *
     * @param count Number of countries
     * @return Countries with highest unemployment rate
     */
    public List<CountryUnemploymentRate> getHighestUnemploymentRate(int count) {
        List<List<String>> reverseOrderedKeys = new ArrayList<>(unemploymentRate.keySet());
        Collections.reverse(reverseOrderedKeys);
        return getCountryList(count, reverseOrderedKeys);
    }

    private List<CountryUnemploymentRate> getCountryList(int count, List<List<String>> keys) {
        List<CountryUnemploymentRate> countryList = new ArrayList<>();
        for (List<String> key : keys) {
            if (count == 0) {
                break;
            }
            String countryCode = key.get(1);

            // we do not care about total unemployment rate of OECD or EU15 countries
            if (!(isCountryInArray(countryCode, countryList) || countryCode.equals("OECD") || countryCode.equals("EU15"))) {
                CountryUnemploymentRate country = new CountryUnemploymentRate(
                        getCountryName(countryCode),
                        countryCode,
                        Integer.parseInt(key.get(2)),
                        unemploymentRate.get(key)
                );
                countryList.add(country);
                count--;
            }
        }
        return countryList;
    }

    private LinkedHashMap<List<String>, Double> convertValuesToDouble(Map<List<String>, Number> oldMap) {
        LinkedHashMap<List<String>, Double> newMap = new LinkedHashMap<>();
        for (List<String> key : oldMap.keySet()) {
            newMap.put(key, oldMap.get(key).doubleValue());
        }
        return newMap;
    }

    private LinkedHashMap<List<String>, Double> sortMapByValue(Map<List<String>, Double> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    private boolean isCountryInArray(String countryCode, List<CountryUnemploymentRate> list) {
        for (CountryUnemploymentRate country : list) {
            if (country.getCountryCode().equals(countryCode)) {
                return true;
            }
        }
        return false;
    }

    private String getCountryName(String countryCode) {
        return dataset.getDimension()
                .get("area")
                .getCategory()
                .getLabel()
                .get(countryCode);
    }
}
