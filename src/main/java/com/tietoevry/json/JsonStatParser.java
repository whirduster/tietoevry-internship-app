package com.tietoevry.json;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import no.ssb.jsonstat.JsonStatModule;
import no.ssb.jsonstat.v2.Dataset;
import no.ssb.jsonstat.v2.DatasetBuildable;

import java.io.IOException;
import java.net.URL;

/**
 * This class encapsulates all methods that parse input JSON file and load it into Dataset object
 *
 * @author Tomas Libal
 */
public class JsonStatParser {

    /**
     * Downloads the JSON-stat from from given URL path
     * @param url Url path to JSON-stat file
     * @return Dataset object with loaded JSON-stat file
     * @throws IOException Error while reading JSON file
     */
    public static Dataset downloadJsonStatFile(String url) throws IOException {
        return createJsonStatMapper()
                .readValue(new URL(url), DatasetBuildable.class)
                .build();
    }

    private static ObjectMapper createJsonStatMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new GuavaModule());
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new JavaTimeModule());
        mapper.registerModule(new JsonStatModule());
        return mapper;
    }
}
