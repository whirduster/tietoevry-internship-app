package com.tietoevry.dataobject;

import java.util.Objects;

/**
 * This class represent the entity of observed country.
 *
 * @author Tomas Libal
 */
public class CountryUnemploymentRate {
    private String countryName;
    private String countryCode;
    private int year;
    private double unemploymentRate;

    public CountryUnemploymentRate(String countryName, String countryCode, int year, double unemploymentRate) {
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.year = year;
        this.unemploymentRate = unemploymentRate;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getUnemploymentRate() {
        return unemploymentRate;
    }

    public void setUnemploymentRate(double unemploymentRate) {
        this.unemploymentRate = unemploymentRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryUnemploymentRate that = (CountryUnemploymentRate) o;
        return year == that.year &&
                Double.compare(that.unemploymentRate, unemploymentRate) == 0 &&
                Objects.equals(countryName, that.countryName) &&
                Objects.equals(countryCode, that.countryCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryName, countryCode, year, unemploymentRate);
    }
}
